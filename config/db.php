<?php

return [
    // [required]
    'type' => 'mysql',
    'host' => env('DB_HOST'),
    'database' => env('DB_DATABASE'),
    'username' => env('DB_USERNAME'),
    'password' => env('DB_PASSWORD'),
 
    // [optional]
    'charset' => 'utf8mb4',
    'collation' => 'utf8mb4_unicode_ci',
    'port' => env('DB_PORT', 3306),
 
    'prefix' => '',
 
    // [optional] Enable logging, it is disabled by default for better performance.
    'logging' => true,
 
    'error' => PDO::ERRMODE_SILENT,
 
    // [optional]
    // The driver_option for connection.
    // Read more from http://www.php.net/manual/en/pdo.setattribute.php.
    'option' => [
        PDO::ATTR_CASE => PDO::CASE_NATURAL
    ],
 
    // [optional] Medoo will execute those commands after connected to the database.
    'command' => [
        'SET SQL_MODE=ANSI_QUOTES'
    ]
];