<?php
namespace EstevanTn\SingleApp;

use Dotenv\Dotenv;
use Medoo\Medoo;

class App 
{
    static $app;


    protected $config;
    public $request;
    public $template;
    public $db;
    public $auth;

    public function __construct()
    {
        error_reporting(E_ALL);

        define('__ROOT__', dirname(dirname(__FILE__)));
        define('__APP_DIR__', dirname(__FILE__));
        define('__SITE_DIR__', __ROOT__.'/app');

        require_once __DIR__.'/helpers/http.php';
        require_once __DIR__.'/helpers/helpers.php';
        require_once __DIR__.'/helpers/app.php';

        $dotenv = Dotenv::createImmutable(__DIR__.'/../');
        $dotenv->load();

        define('__APP_VERSION__', env('APP_VERSION', '1.0.0'));

        session_start();
    }

    public function init()
    {
        $this->db = new Medoo(include __ROOT__.'/config/db.php');
        $this->request = new Request();
        $this->auth = new Auth();
        $this->template = new Template($this->request->getPath());
        echo $this->template->html();
    }

    public function __call($method, $args) 
    {
        return call_user_func_array([$this, $method], $args);
    }

    public static function instance() : App
    {
        if(is_null(static::$app)) {
            static::$app = new App();
        }

        return static::$app;
    }

    public static function start()
    {
        static::instance()->init();
    }
}