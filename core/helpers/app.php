<?php

use Medoo\Medoo;
use EstevanTn\SingleApp\App;
use EstevanTn\SingleApp\Auth;

if(!function_exists('app')) {
    function app() : App
    {
        return App::instance();
    }
}

if(!function_exists('add_var')) {
    function add_var(string $name, $value = null)
    {
        app()->template->addVar($name, $value);
    }
}

if(!function_exists('add_meta')) {
    function add_meta(string $name, ?string $content = null)
    {
        app()->template->addMeta($name, $content);
    }
}

if(!function_exists('set_title')) {
    function set_title(string $title)
    {
        app()->template->setTitle($title);
    }
}

if(!function_exists('set_layout')) {
    function set_layout(string $name)
    {
        app()->template->setLayout($name);
    }
}

if(!function_exists('db')) {
    function db() : Medoo
    {
        return app()->db;
    }
}

if(!function_exists('auth')) {
    function auth() : Auth
    {
        return app()->auth;
    }
}