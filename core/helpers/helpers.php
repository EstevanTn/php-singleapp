<?php

if (!function_exists('env')) {
    function env(string $name, $default = null)
    {
        return isset($name) ? $_ENV[$name] : $default;
    }
}

if(!function_exists('url')) {
    function url(string $uri, array $params = []) : string
    {
        $baseUrl = env('BASE_URL', 'http://localhost');
        $server = parse_url($baseUrl);
        $parts = [
            'scheme'    =>  (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http"),
            'host'  =>  $server['host'],
            'path'  =>  $server['path'].$uri,
        ];

        if(count($params) > 0) {
            $parts = array_merge($parts, [
                'query' =>  http_build_query($params),
            ]);
        }

        return http_build_url($baseUrl, $parts);
    }
}

if(!function_exists('asset')) {
    function asset(string $uri, ?string $version = null) : string
    {
        $parts = explode('::', $uri, 2);

        if(count($parts) === 2) {
            $uri = '/'.$parts[0].'/assets'.$parts[1];
        } else {
            $uri = '/assets'.$parts[0];
        }

        return url($uri, empty($version) ? [] : [ 'v' => $version, ]);
    }
}

if(!function_exists('current_url')) {
    function current_url() : string
    {
        return (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    }
}

if(!function_exists('namespace_file')) {
    function namespace_file($name) : string
    {
        $parts = explode('::', $name, 2);

        if(count($parts) === 1) {
            $parts[] = $parts[0];
            $parts[0] = basename(__SITE_DIR__);
        }

        $name = ($parts[0].'.'.$parts[1]);
        return '/'.str_replace('.', '/', $name).'.php';
    }
}

if(!function_exists('route')) {
    function route($name, array $params = []) : string
    {
        return url('/index.php', array_merge($params, [
            'path'  =>  base64_encode(namespace_file($name)),
        ]));
    }
}

if(!function_exists('has_cookie')) {
    function has_cookie($name) : bool
    {
        return isset($_COOKIE[$name]);
    }
}

if(!function_exists('has_session')) {
    function has_session($name) : bool
    {
        return isset($_SESSION[$name]);
    }
}

if(!function_exists('str_random'))
{
    function str_random($length = 15) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $charactersLength = strlen($characters);
            $randomString = '';
            for ($i = 0; $i < $length; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }
        return $randomString;
    }
}

if(!function_exists('slug')) {
    function slug($string) {
        $characters = [
            "Á" => "A", "Ç" => "c", "É" => "e", "Í" => "i", "Ñ" => "n", "Ó" => "o", "Ú" => "u",
            "á" => "a", "ç" => "c", "é" => "e", "í" => "i", "ñ" => "n", "ó" => "o", "ú" => "u",
            "à" => "a", "è" => "e", "ì" => "i", "ò" => "o", "ù" => "u"
        ];

        $string = strtr($string, $characters);
        $string = strtolower(trim($string));
        $string = preg_replace("/[^a-z0-9-]/", "-", $string);
        $string = preg_replace("/-+/", "-", $string);

        if(substr($string, strlen($string) - 1, strlen($string)) === "-") {
            $string = substr($string, 0, strlen($string) - 1);
        }

        return $string;
    }
}