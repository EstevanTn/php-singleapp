<?php
namespace EstevanTn\SingleApp;

class Auth
{
    protected $sessionName;
    protected $cookieName;

    protected $table;
    protected $user;

    public function __construct()
    {
        $this->table = 'users';
        $this->sessionName = 'single_app:session';
        $this->cookieName = 'single_app:cookie-'.str_random();
    }

    public function attempt(array $credentials, $columns = '*') : bool
    {
        $user = db()->get($this->table, $columns, $credentials);

        if(is_null($user))
            return false;

        $this->setUser($user);
        
        return true;
    }


    public function setUser(array $user)
    {
        $_SESSION[$this->sessionName] = $this->cookieName;
        setcookie($this->cookieName, json_encode($user));
    }

    public function check() : bool
    {
        return has_session($this->sessionName);
    }

    public function logout()
    {
        session_destroy();

        unset($_SESSION[$this->sessionName]);
        unset($_COOKIE[$this->cookieName]);
    }
}