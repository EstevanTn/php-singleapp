const toggleClass = (selector, className) => {
    let elements = document.querySelectorAll(selector)
    elements.forEach((el) => {
        el.addEventListener('click', () => {
            let target = document.querySelector(el.getAttribute('data-target'))
            target.classList.toggle(className)
        })
    })
}

document.addEventListener('DOMContentLoaded', () => {
    toggleClass('[data-event="toggle"]', 'hidden')
})