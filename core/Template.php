<?php
namespace EstevanTn\SingleApp;

use RuntimeException;

class Template
{
    protected $config;
    protected $replaces;

    public function __construct(string $view = null)
    {
        $this->config = [
            '__default__'   => __APP_DIR__.'\\views\\app.layout.php',
            'path'  =>  null,
            'view'  =>  empty($view) ? __ROOT__.namespace_file('app::index') : __ROOT__.$view,
            'variables' => [
                'author' => 'Estevan Tn',
                'app_name' => env('APP_NAME', 'Application'),
                'app_version' => env('APP_VERSION', '1.1.1'),
                'app_year' => env('APP_YEAR', '2023'),
                'title' =>  'Bienvenido',
            ],
            'meta' => [
                'author' => 'Estevan Tn',
            ],
            'plugins' => [
                ['type' => 'style', 'src' => asset('core::/css/all.min.css'), 'in_head' => true],
                ['type' => 'style', 'src' => asset('core::/css/hint.min.css'), 'in_head' => true],
                ['type' => 'script', 'src' => 'https://cdn.tailwindcss.com', 'in_head' => true],
                ['type' => 'script', 'src' => 'https://cdnjs.cloudflare.com/ajax/libs/axios/0.21.4/axios.min.js', 'in_head' => true],
                ['type' => 'script', 'src' => asset('core::/js/app.js'), 'in_head' => false],
            ],
            'sections' => [],
        ];
        $this->replaces = [];
    }

    public function addVar(string $name, $value = null)
    {
        $this->config['variables'][$name] = $value;
    }

    public function addMeta(string $name, ?string $value = null)
    {
        $this->config['meta'][$name] = $value;
    }

    public function addSection(string $name, ?string $content = null)
    {
        $this->config['sections'][$name] = $content;
    }

    public function addReplace(string $name, ?string $content)
    {
        $this->replaces[$name] = $content;
    }

    public function addPlugin(string $src, ?string $type = 'style', ?bool $inHead = false)
    {
        $type = trim(strtolower($type));

        if(!in_array($type, ['style', 'script']))
            throw new RuntimeException('Solo se permite los valores: \'style|script\'');

        $this->config['plugins'] = [
            'src' => $src,
            'type'  =>  $type,
            'in_head' =>  $type === 'style' ? true : $inHead,
        ];
    }

    public function setTitle(string $title)
    {
        return $this->addVar('title', $title);
    }

    public function setLayout(string $name)
    {
        $path = __SITE_DIR__.'/'.str_replace('.', '/', $name).'.layout.php';

        if(!file_exists($path))
            throw new RuntimeException('El layout que desea usar no está definido.');

        $this->config['path'] = $path;
    }

    public function html() : string
    {
        $html = $this->getView($this->config['view']);

        $metaHtml = '';
        foreach ($this->config['meta'] as $key => $value) {
            $metaHtml .= '<meta name="'.$key.'" content="'.$value.'" />';
        }
        $this->addSection('meta', $metaHtml);

        foreach ($this->config['variables'] as $key => $value) {
            $this->addReplace('@@'.strtolower($key), $value ?? '');
        }

        foreach ($this->config['sections'] as $key => $value) {
            $this->addReplace('__'.strtoupper($key).'__', $value ?? '');
        }

        $initPage = '';
        $endPage = '';
        foreach ($this->config['plugins'] as $plugin) {
            if($plugin['type'] === 'style')
                $node = '<link rel="stylesheet" href="'.$plugin['src'].'" />';
            else
                $node = '<script src="'.$plugin['src'].'"></script>';
            
            if($plugin['in_head'])
                $initPage .= $node;
            else
                $endPage .= $node;
        }

        foreach ($this->replaces as $key => $value) {
            $html = str_replace($key, $value, $html);
        }

        $this->addReplace('__INIT_PAGE__', $initPage);
        $this->addReplace('__CONTENT__', $html);
        $this->addReplace('__END_PAGE__', $endPage);
        
        $layout = $this->getView(empty($this->config['path']) ? $this->config['__default__'] : $this->config['path']);

        foreach ($this->replaces as $key => $value) {
            $layout = str_replace($key, $value, $layout);
        }
        
        return $layout;
    }

    public function getView(string $path) : string
    {
        ob_start();
        if(file_exists($path))
            include $path;
        else
            include __APP_DIR__.'\\views\\errors\\404.php';
        $html = ob_get_contents();
        ob_end_clean();

        return $html;
    }
}