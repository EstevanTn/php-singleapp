<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@@title | @@app_name</title>
    __META__
    <link rel="shortcut icon" href="<?= asset('/favicon.ico') ?>" type="image/x-icon">
    __INIT_PAGE__
    <script>
        var baseAppUrl = '<?= url('/') ?>';
    </script>
</head>
<body class="fixed bg-white w-full h-screen text-gray-800">
    <div id="app" class="absolute flex flex-col h-full w-full">
        <div class="flex flex-col w-full h-full overflow-y-auto">
            <div class="bg-white md:sticky z-40 md:top-0 border-b border-gray-300 shadow">
                <div class="flex flex-col lg:flex-row w-full md:max-w-3xl lg:max-w-4xl m-auto justify-between px-5 py-3">
                    <div class="inline-flex space-x-4">
                        <button data-event="toggle" data-target="#nav-menu" type="button" class="w-16 border rounded-md lg:rounded-lg lg:hidden hover:bg-gray-200">
                            <i class="fa fa-bars m-auto text-xl"></i>
                        </button>
                        <div class="w-full flex items-center">
                            <a href="<?= route('index') ?>" class="w-full">
                                <img src="<?= asset('core::/images/logo.png') ?>" class="h-12" alt="">
                            </a>
                        </div>
                    </div>
                    <div id="nav-menu" class="hidden flex lg:flex flex-col lg:flex-row pt-3 lg:pt-0 space-y-2 lg:space-y-0 lg:space-x-4">
                        <?php if(auth()->check()): ?>
                        <a class="px-5 py-4 bg-red-500 hover:bg-red-600 text-gray-100 hover:text-white cursor-pointer rounded hint--bottom" href="<?= route('index', ['action' => 'logout']) ?>" aria-label="Cerrar Sesión">
                            <i class="fal fa-sign-out"></i>
                            <span class="ml-2 lg:hidden">Cerrar Sesión</span>
                        </a>
                        <?php else: ?>
                        <a class="px-5 py-4 bg-red-500 hover:bg-red-600 text-gray-100 hover:text-white cursor-pointer rounded hint--bottom" href="<?= route('login') ?>" aria-label="Iniciar Sesión">
                            <i class="fal fa-sign-in"></i>
                            <span class="ml-2 lg:hidden">Iniciar Sesión</span>
                        </a>
                        <?php endif; ?>
                    </div>                
                </div>
            </div>
            <div class="flex-grow bg-gray-100">__CONTENT__</div>
            <div class="border-t border-gray-300">
                <div class="w-full md:max-w-3xl lg:max-w-4xl py-2 m-auto">
                    <div class="text-gray-700 text-xs text-center">MPS © @@app_year, GERENCIA DE TECNOLOGÍA DE INFORMACIÓN Y COMUNICACIÓN | CHIMBOTE PERÚ.</div>
                </div>
            </div>
        </div>
    </div>
    __END_PAGE__
</body>
</html>