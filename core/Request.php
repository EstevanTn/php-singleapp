<?php
namespace EstevanTn\SingleApp;

class Request
{
    protected $inputs;

    public function __construct()
    {
        $input = json_decode(file_get_contents("php://input"), true);
        $this->inputs = array_merge(is_null($input) ? [] : $input, is_null($_REQUEST) ? [] : $_REQUEST);
    }

    public function has(string $name) : bool
    {
        return isset($this->inputs[$name]);
    }

    public function is(string $method) : bool
    {
        return $_SERVER['REQUEST_METHOD'] === strtoupper($method);
    }

    public function get(string $name, $default = null) : mixed
    {
        return $this->has($name) ? $this->inputs[$name] : $default;
    }

    public function getPath() : ?string
    {
        return empty($this->get('path')) ? $this->get('path') : base64_decode($this->get('path'));
    }
}