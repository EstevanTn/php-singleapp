<?php

use EstevanTn\SingleApp\App;

require_once __DIR__.'/vendor/autoload.php';

App::start();